import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AjaxService } from '../service/ajax.service';
import { switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user-account',
  templateUrl: './create-user-account.component.html',
  styleUrls: ['./create-user-account.component.scss']
})
export class CreateUserAccountComponent implements OnInit {

  adForm: FormGroup;
  constructor(private formBuilder: FormBuilder, private ajaxUser: AjaxService, private route:Router) {
    this.ngOnInit()
  }
  ngOnInit() {
    this.adForm = this.formBuilder.group({
      surname: '',
      name: '',
      email: '',
      password: '',
    })

  }


  addUserSubmit() {

    console.log(typeof (this.adForm));
    console.log(this.adForm);

    this.ajaxUser.addUser(this.adForm.value).pipe(

      switchMap(() => this.ajaxUser.login(this.adForm.value.email, this.adForm.value.password))

    ).subscribe(() => this.route.navigate(['/user-admin']));
  }

}
