import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AjaxService } from '../service/ajax.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.scss']
})
export class AccountFormComponent implements OnInit {

  AccountForm: FormGroup

  constructor(private formBuilder:FormBuilder, private ajaxService:AjaxService, private route:Router) {
    this.initForm()
  }

  ngOnInit() {
  }

  initForm() {
    this.AccountForm = this.formBuilder.group({
      'name': ['', [Validators.required, Validators.maxLength(20)]],
      'bank': ['', [Validators.required, Validators.maxLength(50)]],
      'balance': [0, Validators.required],
    })
  }
  onSubmitForm() {
    const value = this.AccountForm.value
    this.ajaxService.addAccount(value).subscribe();
    return this.route.navigate(['/user-admin'])
  }
}



