import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'localNumber'
})
export class LocalNumberPipe implements PipeTransform {

  transform(number: number, local: string = "uk"): any {
    return number.toLocaleString(`${local}`);
  }

}
