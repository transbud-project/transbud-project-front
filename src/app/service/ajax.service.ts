import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { Operation } from '../entity/operation';
import { Account } from '../entity/account';
import { Category } from '../entity/category';
import { tap } from "rxjs/operators";
import * as jwtdecode from "jwt-decode";
import { UserComponent } from '../user/user.component';

@Injectable({
  providedIn: 'root'
})
export class AjaxService {

  private urlOperation = '/api/operation/'
  private urlAccount = '/api/user/account/'
  private urlCategory = '/api/categories'
  private urlAuth = '/api/login_check'
  private urlUser = '/api/user'

  user = new BehaviorSubject<any>(null);
  
  constructor (private http:HttpClient) {
    
    if(localStorage.getItem('token')) {
      this.user.next(
        jwtdecode(localStorage.getItem('token'))
        );
    }
  }

  getAllOperation():Observable<Operation[]>
  {
    return this.http.get<Operation[]>(this.urlOperation)
  }

  addOneOperation(operation:Operation, id):Observable<Operation>
  {
    return this.http.post<Operation>(`${this.urlAccount}${id}/operation/`,operation)
  }

  removeOneOperation(operation:Operation, idAccount:number):Observable<any>
  {
  
    return this.http.delete(`${this.urlAccount}${idAccount}/operation/${operation.id}`)
  }

  getAllAccount():Observable<Account[]>
  {
    return this.http.get<Account[]>(this.urlAccount);
  }

  getOneAccount(id:number):Observable<Account>
  {
    return this.http.get<Account>(`${this.urlAccount}${id}`);
  }

  getAllOperationByAccount(id) {

    return this.http.get<Operation[]>(`${this.urlAccount}${id}/operation`);
  }

  getAllCategories():Observable<Category[]>
  {
     return this.http.get<Category[]>(`${this.urlCategory}`)

  }


  addUser(user:UserComponent):Observable<UserComponent>
  {
    return this.http.post<UserComponent>(`${this.urlUser}`,user)
  }

  getUserAccount():Observable<Account[]>
  {
    return this.http.get<Account[]>(`${this.urlUser}`);
  }
  
  login (email:string, password:string): Observable<any> {
    console.log('service > login')
    return this.http.post<any>(this.urlAuth, {
    username:email, 
    password:password
    }).pipe(
      
      tap(response => {
        console.log('response', response)
        if(response.token) {
          
          localStorage.setItem('token', response.token);
         
          this.user.next(jwtdecode(response.token));
        }
      })
    );
  }
 
  logout() {
    localStorage.removeItem('token');
    this.user.next(null);
  }
  
  isLoggedIn():boolean {
    if(localStorage.getItem('token')) {
      return true;
    }
    return false;
  }

    addAccount(account:Account):Observable<Account>{
    return this.http.post<Account>(`${this.urlAccount}`,account)
  }

}


//   private operationUrl :string = 'http://localhost:8080/operation/';
//   private accountUrl  = 'http://localhost:8080/user/account/';


//   constructor(private http:HttpClient) { }
//   getAllOperation(): Observable<Operation[]> {
//     return this.http.get<Operation[]>(this.operationUrl)
//   }

//   addOperation(operation:Operation, id):Observable<Operation>{
//     return this.http.post<Operation>(`${this.accountUrl}${id}/operation`,operation);
//   }

//   deleteOperation(operation:Operation, idAccount:number):Observable<any>{
//     return this.http.delete(`${this.accountUrl}${idAccount}/operation/${operation.id}`);
//   }

//   getAllOperationByAccount(id){
//     return this.http.get<Operation[]>(`http://localhost:8080/user/account/${id}/operation`);
//   }

//   public getAllAccount():Observable<Account[]>
//   {
//     return this.http.get<Account[]>(this.accountUrl);
//   }

//   public getOneAccount(id){
//     return this.http.get<Account>(`${this.accountUrl}${id}`);
//   }

//   public AddAccount(account:Account):Observable<Account>{
//     return this.http.post<Account>(`${this.accountUrl}`,account)
//   }
// }
