import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
library.add(fas);
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule} from '@angular/forms';
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatIconModule } from '@angular/material/icon';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BannerComponent } from './banner/banner.component';
import { UserComponent } from './user/user.component';
import { AccountCreationComponent } from './form/account-creation/account-creation.component';
import { AboutComponent } from './about/about.component';
import { FooterComponent } from './footer/footer.component';
import { AccountComponent } from './account/account.component';
import { HomeComponent } from './home/home.component';
import { AccountFormComponent } from './account-form/account-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FeaturesComponent } from './features/features.component';
import { PartnersComponent } from './partners/partners.component';
import { LoginComponent } from './login/login.component';
import { CreateUserAccountComponent } from './create-user-account/create-user-account.component';
import { UserAdminComponent } from './user-admin/user-admin.component';

import { DateFromApiPipe } from './pipes/date-from-api.pipe';
import { AuthorizationInterceptor } from './service/interceptor/authorization.service';
import { LocalNumberPipe } from './pipes/local-number.pipe';

const routes: Routes = [
  { path: 'user/account/:id', component: AccountComponent },
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'create-user-account', component: CreateUserAccountComponent},
  { path: 'user-admin', component: UserAdminComponent},
  { path: 'addaccount', component:AccountFormComponent },
  { path: 'user', component: UserComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BannerComponent,
    AboutComponent,
    AccountCreationComponent,

    FooterComponent,
    AccountComponent,
    HomeComponent,
    AccountFormComponent,
    FeaturesComponent,
    PartnersComponent,
    LoginComponent,
    CreateUserAccountComponent,
    UserAdminComponent,
    UserComponent,

    DateFromApiPipe,

    LocalNumberPipe


  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule, 
    FontAwesomeModule,
    HttpClientModule,
    FormsModule,
    MatFormFieldModule,
    HttpClientModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthorizationInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
