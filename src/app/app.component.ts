import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Budget';
  pageCssClass: string;

  constructor(private router: Router) {}

  ngOnInit() {
    this.router.events.subscribe((event) =>  {
      if (event instanceof NavigationStart) {
          this.pageCssClass = event.url === '/' ? 'page-home' : '';
        }     
    });
  }
}
