import { Component, OnInit } from '@angular/core';
import { AjaxService } from '../service/ajax.service';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  adForm:FormGroup
  isLogged:Observable<any>;

  constructor(private ajaxService:AjaxService, private formBuilder:FormBuilder, private route:Router) { 
    // this.login()
    this.initForm()
  }

  ngOnInit() {
    this.isLogged = this.ajaxService.user;
   
    
  }



  initForm(){
    console.log('initform')
    this.adForm= this.formBuilder.group({
        email:'',
        password:''
    })
  }

  login() 
  {
    console.log('login')
    console.log(` component : ${this.adForm.value.email} pour ${this.adForm.value.password}`)
    // const value = this.adForm.value
    const username = this.adForm.value.email;
    const password = this.adForm.value.password;
    this.ajaxService.login(username, password).subscribe(
      (response) => this.route.navigate(['/user-admin']),
      (error) => console.log,
      );

    
  
  }
  logged() {
    return this.ajaxService.isLoggedIn();
  }

  logout(){

    this.ajaxService.logout();
    this.route.navigate(['/login']);

    
  }
}
