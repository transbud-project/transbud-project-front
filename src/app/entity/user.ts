export interface User {

  id?:number;
  name?:string;
  surname?:string;
  email?:Text;
  password?:string;
  accounts:Account[];
  
}
