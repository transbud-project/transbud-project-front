import { Category } from "./category";

export interface Operation {
  id:number;
  amount:number;
  account:Account;
  date:Date;
  category:Category;
}
