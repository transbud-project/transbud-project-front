import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery'
import { AjaxService } from '../service/ajax.service';
import { faUserCircle, faUser } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  // fa icons
  faUser = faUserCircle;

  
  constructor(private ajaxService:AjaxService, private route:Router) { 
 
  }

  ngOnInit() {
    
    
  }

  logged() {
    return this.ajaxService.isLoggedIn();
  }

  logout(){
    this.ajaxService.logout();
    this.route.navigate(['/']);
  }

  }

