import { Component, OnInit } from '@angular/core';
import { Operation } from '../entity/operation';
import { Account } from '../entity/account';
import { AjaxService } from '../service/ajax.service';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment'
import { Observable } from 'rxjs';
import { Category } from '../entity/category';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  public operations: Operation[] = [];
  public newOperation: Partial<Operation> = { "amount": 0 };

  public accounts: Array<Account> = [];
  public account: Account;

  categories:Category[] = [];

  public accountId: number = 0;

  // sum of operation by type
  public debit = 0;
  public stat = {
    "debit": 0,
    "credit": 0
  }

  constructor(route: ActivatedRoute, private ajaxService: AjaxService) {
    route.params.subscribe(data => this.accountId = +data['id']);
  }

  ngOnInit() {

    this.ajaxService.getAllOperationByAccount(this.accountId).subscribe(
      data => {this.operations = data 
        },
      error => console.error(error.message),
      () => console.info(`findAll() operation success for account id : ${this.accountId}`)
    )

    this.ajaxService.getAllAccount().subscribe(
      data => this.accounts = data,
      error => console.error(error.message),
      () => console.info("findAll() account success")

    )
    this.ajaxService.getAllCategories().subscribe(
      categories => this.categories= categories,
      error => console.error(`error : get all category ${error}`)

    )
    this.ajaxService.getOneAccount(this.accountId).subscribe(
      data => this.account = data
    )
  }

  public add(operation) {
    operation.amount = Number(operation.amount);
    this.ajaxService.addOneOperation(operation, this.accountId).subscribe(
      () => this.ngOnInit(),
      error => console.error(`Add One operation : ${error.message}`),
      () => console.info(`add an operation name:${operation.amount}`)
    )
  }

  public remove(operation) {

    this.ajaxService.removeOneOperation(operation, this.accountId).subscribe(
      () => this.ngOnInit(),
      error => console.error(error.mesage),
      () => console.info(`remove operation id : ${operation.id}`)
    );
  }

  public computeStat() {

    for (const operation of this.operations) {
      if(operation.amount < 0){
        this.stat["debit"] += operation.amount; 
      }
      else {
        this.stat['credit'] += operation.amount;
      }
    }
    
  } 

  operationColor(amount:number) {
    if (amount < 0) {
      return '.nico';
    }
  }
}



//   operations:Operation[] = [];
//   accounts:Array<Account> = [];
//   account:Account;

//   newOperation:Partial<Operation> = {"amount":0}; 

//   id:number = 0;

//   constructor(route: ActivatedRoute, private service:AjaxService) {
//     route.params.subscribe(data => this.id = +data['id']);
//    }

 
//   ngOnInit() {

//     this.service.getAllAccount().subscribe(
//       data => this.accounts = data,
//       error => console.error(error.message),
//       () => console.info("findAll() account success")
//       )

//     this.service.getOneAccount(this.id).subscribe(
//       data => this.account = data,
//       error => console.error(error.message),
//       () => console.info("getOneAccount() operation success")
//     )
    
//     this.service.getAllOperationByAccount(this.id).subscribe(
//       data => this.operations = data,
//       error => console.error(error.message),
//       () => console.info("findAll() operation success")
//     )
//   }

//   public add(operation){
//     this.service.addOperation(operation, this.id).subscribe(
//       ()=>this.ngOnInit(),
//       error => console.error(error.message),
//       ()=>console.info("add on operation name:${operation.name}")
//     )
//   }

//   public delete(operation){
//     this.service.deleteOperation(operation, this.id).subscribe(
//       ()=>this.ngOnInit()
//     )
//   };
  

// }
