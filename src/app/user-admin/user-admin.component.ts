import { Component, OnInit } from '@angular/core';
import { AjaxService } from '../service/ajax.service';
import { Account } from '../entity/account';

@Component({
  selector: 'app-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.scss']
})
export class UserAdminComponent implements 


OnInit {

  accounts:Account[] = [];
  constructor(private service:AjaxService) { }

  ngOnInit() {
    this.service.getUserAccount().subscribe(
      accounts => this.accounts = accounts
    );

  }

}
